# Computer Controlled Cutting

## Goal of this unit

In this unit, you will learn how to design, prepare 2D CAD files and lasercut them.

## Before class

Install 2D vector imagery tool :

* [Inkscape](https://inkscape.org/)


## Class materials

* [Laser Cutters](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)

### Some inspirations in Science & Design

* [Using origami to make a lamp - Design](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/clementine.benyakhou/final-project/)
* [Using origami design principles to fold reprogrammable mechanical metamaterials](https://science.sciencemag.org/content/345/6197/647/)
* [Bioinspired kirigami metasurfaces as assistive shoe grips - shared paper](https://www.nature.com/articles/s41551-020-0564-3.epdf?sharing_token=5dzLWfJga3eT3-gFXxiNktRgN0jAjWel9jnR3ZoTv0NAsHNqBhhyoMUKvOwEuu9nASx3y7Yr5GzbISiqhVqv4EwS1tH6wCO8RZX8qquTDSw8b2Xg8Kn-fRCK--dSr3kLjtTiBRpNjNxAwL0M-Lp--Wk19ylCwGTkA6xwHkrdieM=)
* [Strengthening paper with kirigami cut patterns](https://journals.aps.org/prx/abstract/10.1103/PhysRevX.10.011013)
* [Super paper](https://phys.org/news/2008-06-super-paper-nanopaper-break-resistant-iron.html)

## Assignment

Group assignment:

* characterize your lasercutter's focus, power, speed, kerf, ... for folding and cutting cardboard paper

Individual assignment:

* design, lasercut, and document a kirigami (cut and folds) to make a 3D object

## Learning outcome

* Demonstrate and describe 2D design processes
* Identify and explain processes involved in using the laser cutter.
* Develop, evaluate and construct the 3D object made from a kirigami
