# Final Projects

## Assignment

You will design, fabricate and document a proof of concept of a scientifically-based and frugal solution to solve a real-world problem you have identified.

* You will position your project along the [17 Sustainable Development Goals](https://sdgs.un.org/) listed by the United Nations.
* You will show scientific evidence with sources and references on which you build on your project.
* You will use at least one digital fabrication technique that is used in a fablab.

## Final projects documentation

| | |
| --- | --- |
|![David Mathy](img/david.mathy-slide.jpg) |**Grass measuring device** <br> [*David Mathy*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/david.mathy/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/david.mathy/final-project/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/david.mathy-presentation.pdf) |
|![Pauline Mackelbert](img/pauline.mackelbert-slide.jpg) | **Cardboard cutter**  <br> [*Pauline Mackelbert*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/final-project/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/pauline.mackelbert-presentation.pdf) |
|![Arié Nabet](img/nabet.arie-slide.jpg) |**Asian hornet selective trap**  <br> [*Arié Nabet*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/Projet%20Final/Prototyping_%26%20pi%C3%A8ge_final/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/arie.nabet-presentation.pdf)|
|![Lionel Compere Leroy](img/lionel.compere.leroy-slide.jpg) |**Mushroom growing box**  <br> [*Lionel Compere Leroy*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/Final_Project/4.%20Prototypage/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/lionel.compere.leroy-presentation.pdf) |
|![slide.jpg](img/alessandro_abbracciante_sajl_ghani-slide.jpg) |**Bicycle helmet lamps**  <br> *[Sajl Ghani](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/) & [Alessandro Abbracciante](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/)* <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/final-project/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/alessandro_abbracciante_sajl_ghani-presentation.pdf) |
|![slide.jpg](img/anais.derue-benjamin.hainaut-slide.jpg) |**Recycling plastic press**  <br> *[Benjamin Hainaut](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/benjamin.hainaut/) & [Anaïs Derue](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/anais.derue/)* <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/benjamin.hainaut/final-project/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/anais.derue-benjamin.hainaut-presentation.pdf) |
|![Andrew Karam](img/andrew.karam-slide.jpg) |**Conscious water consumption**  <br> [*Andrew Karam*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/andrewkaram/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/andrewkaram/final-project/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/andrew.karam-presentation.pdf) |
|![Alexandre Stoesser](img/stoesser.alexandre-slide.jpg) |**Solar shower**  <br> [*Alexandre Stoesser*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/final-project/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/stoesser.alexandre-presentation.pdf) |
|![Nathalie Wéron](img/nathalie.weron-slide.jpg) |**Closing the human nutrient cycle**  <br> [*Nathalie Wéron*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/Final-project/5.usage/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/nathalie.weron-presentation.pdf) |
|![Esteban Welschen](img/esteban.welschen-slide.jpg) |**Anti-drug drinking protection**  <br> [*Esteban Welschen*](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/esteban.welschen/) <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/esteban.welschen/final-project/) <br> * [final presentation](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/tree/main/docs/final-projects/img/esteban.welschen-presentation.pdf) |



## Preparation for the final presentation

For the final presentation, you will prepare

* a graphical abstract illustrating your question and project (1920x1080px PNG image entitled "firstname.surname-slide.png", [examples here](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/final-projects/final-projects/)) with :
    * a photo/image illustrating your project
    * [a logo of the FabLab ULB](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/tree/main/vade-mecum/images)
    * a short and clear question that your project tries to answer
* a pdf presentation (10 slides/5min for solo project and 16 slides/8min for groups of 2) to share your project with an interdisciplinary audience ("firstname.surname-presentation.pdf").
* prototypes, proof of concept and technical/scientific documents that illustrate your process and your final achievement.
* a printed QR code linking to your online documentation
