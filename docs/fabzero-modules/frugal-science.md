# Frugal Science

## Goal of this unit

The goal of this class is to learn to **design**, **fabricate** and **document** a scientifically-based and frugal project to solve a problem that you have identified and care about.


## Class content

* ULB - How To Make (almost) Any Experiment / FabZero Inside  
  * [2020-2021](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/)
  * [2021-2022](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/)
* [Social entrepreneurs and frugal science, Denis Terwagne's blog post](https://dtwg4.github.io/blog-flexible-jekyll/entrepreneuriat-social-et-science-frugale/)
* [BioE271: Frugal Science, Stanford University, USA](https://www.frugalscience.org/)
* [Digital Fabrication in Cuba - FabLab Studio, ULB, Belgium](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/)


## Assignment

* Draft your personal idea board on your final project page
* For each idea identify
    * Why do you care about this problem ? (Anecdote, ...)
    * What is the problem statement (without thinking of any solution) ?
    * Document some data and scale on the problem.
