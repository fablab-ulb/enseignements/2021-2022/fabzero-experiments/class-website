# Computer-Aided-Design

## Goal of this unit

In this unit, you will learn to

* Learn about compliant mechanism and flexures
* Evaluate and select 3D software
* Demonstrate and describe processes used in modeling with 3D software

## Before class

Install 3D design CAD tools :

* [OpenSCAD](https://openscad.org/)
* [FreeCAD](https://www.freecadweb.org/)

## Class materials

* [Compliant Mechanisms & flexures](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/blob/main/vade-mecum/compliant-mechanisms.md)
* [OpenSCAD tutorial](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/OpenSCAD.md)
* [FreeCAD tutorial](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/FreeCAD.md)

## Assignment

* Design, model, and document a parametric [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks) construction kit that you will fabricate next week using 3D printers.
* Make it parametric, so you will be able to make adjustments accounting for the material properties and the machine characteristics.
