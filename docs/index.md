# ULB - How To Make (almost) Any Experiments / FabZero inside

We live in a very challenging time of rapid change and uncertainties. The United Nations have made a list of [17 Sustainable Development Goals](https://sdgs.un.org/) that we, as a world society should urgently act on in the next decade to make a more equitable and sustainable society.

There are many strategies that we can adopt and many ways our society can evolve. Rob Hopkins [^1] who has founded the transition town and Riel Miller [^2] at the UNESCO urge us to **dream, imagine, design and build the future we desire**.

In this class, you will take foot as a **social inventor and entrepreneur**. You will join an interdisciplinary community to **tackle a challenge you care about**.

![](./img/fablab-machine-logos.svg)

We will work at the crossroads of disciplines and community movements:

* **the digital fabrication revolution, the rise of fablabs and the maker movement**. In this class, we learn to use generic tools and workflows that are common in Fablabs connected to a wide interdisciplinary community and network. This allows us to develop and design projects globally and collaboratively and to fabricate locally.

* the **Frugal Science movement** which goal is to solve planetary scales problems using cost-effective scientifically based solutions that are scalable to meet the problem scale. You will learn the importance of basic science, tinkering and creative play to tackle design challenges.

* **the rise of creative, practicing and learning communities** that are able to adapt, collaborate and solve problems. This class will mix undergrad students from different background that will team up with global collaborators and mentors all around the world to solve the identified challenges.

## Class scenario

In this class, you will start by **identifying a set of problems that you are passionate about**.

You will **learn to use digital fabrication** that you can find in a makerspace or a fablab to build experiments or scientific tools.

Supported by mentors, you will **design, fabricate and document** a scientifically-based and frugal proof of concept of a project to solve the problem that you have identified.

## Learning Community

### ULB students

* [Alessandro ABBRACCIANTE](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/)
* [Mats BOURGEOIS](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/mats.bourgeois/)
* [Lionel COMPERE LEROY](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/lionel.compereleroy/)
* [Sajl GHANI](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/)
* [Benjamin HAINAUT](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/benjamin.hainaut/)
* [Andrew KARAM](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/andrewkaram/)
* [Pauline MACKELBERT](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/)
* [David MATHY](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/david.mathy/)
* [Arié	NABET](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/)
* [Alexandre STOESSER](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/)
* [Nathalie WÉRON](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/nathalie.weron/)
* [Anaïs DERUE](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/anais.derue/)
* [Esteban WELSCHEN](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/esteban.welschen/)

### FabZero-Experiments Teaching Designer and Coordinator

* Denis Terwagne ([Fab Academy](http://archive.fabacademy.org/archives/2017/woma/students/238/), [Frugal Lab](http://frugal.ulb.be/), [FabLab ULB](http://fablab-ulb.be/))

### FabZero Mentors

* [Gwendoline Best](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/gwendoline.best/)
* [Hélène Bardijn](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/helene.bardijn/)
* [Nicolas De Coster](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/)
* [Axel Cornu](https://fabacademy.org/2019/labs/ulb/students/axel-cornu/about/index.html)

## Archives

* [2020-2021 ULB FabZero-Experiments Class Website](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/)

## References

 [^1]: *From What Is to What If: Unleashing the Power of Imagination to Create the Future We Want*, Chelsea Green Publishing Co, 2019
 [^2]: Resilience Frontiers. Riel Miller. A futures Literacy Laboratory @UNESCO ([video](https://www.youtube.com/watch?v=_WgvTfR7TLI))
