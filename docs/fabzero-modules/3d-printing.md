# 3D printing

## Goal of this unit

In this unit, you will learn to

* Learn about code licenses and how to use them.
* Identify the advantages and limitations of 3D printing.
* build on other people's code and give proper credits.
* Apply design methods and production processes using 3D printing.

## Before class

* Install the [slicing software](https://en.wikipedia.org/wiki/Slicer_(3D_printing)) : [PrusaSlicer](https://help.prusa3d.com/en/article/install-prusaslicer_1903) on your computer.

## Class materials


* [3D printing](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md)  
   * [torture test](https://www.thingiverse.com/thing:2806295)  
   * [Design rules]( https://help.prusa3d.com/en/article/modeling-with-3d-printing-in-mind_164135)  
   * [Materials](https://blog.prusaprinters.org/advanced-filament-guide_39718/)  

* Licensing you work
  * [Creative Commons Licence](https://creativecommons.org/about/cclicenses/)
    * [Writing code guidelines](https://integrity.mit.edu/handbook/writing-code)
    * [licensed code example](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/blob/master/PFC-Headband-Light-3DPrint/anti_projection.scad)


## Assignment

Group assignment:

* Test the design rules, comment on the choice of materials and good practices for your 3D printer(s)

Individual assignment:

* Choose an open-source licence such as a [Creative Commons Licence](https://creativecommons.org/about/cclicenses/) or a [MIT Licence](https://en.wikipedia.org/wiki/MIT_License) and adapt the code that you made on the [computer-aided design week](./computer-aided-design/).
* Complete your FlexLinks kit that you made by fetching the code of at least 2 parts made by other classmates during week 2. Acknowledge their work. Modify their code and get the parts ready to print.
* 3D print your flexible construction kit, make a compliant mechanism out of it with several hinges that do something,
* Make and post a (compressed <10 Mo) video of your working mechanism.


