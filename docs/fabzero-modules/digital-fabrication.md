# Digital Fabrication

## Goal of this unit

The goal of this unit is to learn about what is digital fabrication and what will be its impact.


## Class content


Digital Fabrication  

* See Denis Terwagne presentation
* [MIT Class : How To Make (almost) Anything (video)](https://www.youtube.com/watch?v=EYqt2d0fOr8)

What is a FabLab ?  

* [What is a fablab in 3 words ? (video)](https://www.youtube.com/watch?v=nOPGJ2VBCPo)
* [Vigyan Ashram, the first fablab in rural India (video)](https://www.youtube.com/embed/tYARqEp0Nbg?start=1606)

What is FabLab ULB ?  

* [Le FabLab ULB, présentation (video)](https://m.youtube.com/watch?v=Ls7SJDOSHSY)
* [Le FabLab ULB, animation (video](https://m.youtube.com/watch?v=VeV0PH7bbSY)

Fab Academy  

* [get inspired](https://www.youtube.com/watch?v=EJX2BesjD38)
* [website](https://fabacademy.org/)

ULB classes / FabZero inside

* [2021-2022 FabZero-Experiments](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/)
* [2021-2022 FabZero-Design](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/class-website/)
* [2020-2021 FabZero-Experiments](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/)
* [2020-2021 FabZero-Design](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/class-website/)
* [2019-2020 FabLab Studio](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/)
