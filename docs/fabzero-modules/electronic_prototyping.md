# Electronic Prototyping

## Goal of this unit

In this unit, you will learn to use a microcontroller development board to prototype electronic applications by programming it and using input and output devices.

## Before class

Install the [Arduino IDE](https://www.arduino.cc/en/software) on your computer.

## Class materials

* [IO modules listing](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/07e9c48531529f84506c966be76957e4dba6ec1c/IO-Modules.md)
* [Arduino Uno Pinout](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/raw/master/img/Arduino-uno-pinout.png)

Get inspired:

* [Open-source Lab](https://www.appropedia.org/Open-source_Lab)
* [Projet CO2 - Mesurer le CO2 pour mieux aérer](http://projetco2.fr/)

## Assignment

* Make a basic exercice using a development board ([examples here](https://www.arduino.cc/en/Tutorial/BuiltInExamples))
* Measure something: add a sensor to a development board and read it. 
* Read the datasheet of the sensor and explain how the physical property relates to the measured results.
* (optional) Make your board do something : add an output device to a development board and program it to do something
