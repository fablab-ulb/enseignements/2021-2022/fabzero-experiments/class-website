# Electronic Production

## Goal of this unit

In this unit, you will learn to fabricate a microcontroller development board, by milling/etching, stuffing and soldering, to prototype electronic applications.

## Class materials

* [Make your own development board](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md)

## Assignment

* Make a development board by stuffing a milled/etched PCB (Printed Circuit Board), test it.
* Make your board do something

## Learning outcome

* Describe the process of stuffing, de-bugging and programming
* Demonstrate correct workflows and identify areas for improvement if required
