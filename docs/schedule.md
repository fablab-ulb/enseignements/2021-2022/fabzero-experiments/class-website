## Class schedule

Thursday **10h30-12h30** and **13h30-15h30** at FabLab ULB.

*important note : The topic of each class is subject to change.*

Feb 17 - Introduction to Frugal Science, Digital Fabrication, Project Management and Documentation  
Feb 24 - Compliant Mechanisms & Flexures, Computer-Aided Design  
Mar 03 - Computer Controlled Cutting  
Mar 10 - 3D Printing   (starting at 10am to 12:30pm)
Mar 17 - Electronic 1 - Prototyping      
Mar 24 - Electronic 2 - Fabrication  

Mar 31 - Final Project Development  
Apr 21 - Final Project Development  
Apr 28 - Final Project Development  
May 05 - Final Project Development   
May 12 - Final Project Development  
May 19 - Final Project Development   

Jun 16 - Final Project Presentation

## Deadlines :  

Apr 7 - Modules complete (1st round)  
Apr 14 - Cross-evaluation complete  
Apr 21 - response to evaluation and modules final updates  
